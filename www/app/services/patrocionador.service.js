(function () {
    'use strict';
    angular
        .module('app')
        .factory('PatrocionadorService', PatrocionadorService);

    PatrocionadorService.$inject = ['$q', 'SettingsHelper', '$timeout', '$http'];
    function PatrocionadorService($q, SettingsHelper, $timeout, $http) {

          var service = {
              getAll: getAll
          };

          return service;

          function getAll() {
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "/app/www/server/form.php",
                  data: $.param({ id: "getAllPatrocionadores"}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
          }

    }

}());