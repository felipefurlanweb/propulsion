﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('IndexController', IndexController)

    IndexController.$inject = ['PatrocionadorService', '$rootScope', '$scope', '$state', 'localStorageService', 'SettingsHelper', 'EmpresaService', 'ItemEmpresaService'];
    function IndexController(PatrocionadorService, $rootScope, $scope, $state, localStorageService, SettingsHelper, EmpresaService, ItemEmpresaService) {
        $scope.empresas = [];
        $scope.empresas2 = [];
        $scope.itens = [];
        $scope.patrocionadores = [];
        $scope.arr1 = [];
        $scope.arr2 = [];
        $scope.searchteste = "";
        $scope.arr3 = [];
        $scope.selectDrop = {};
        $scope.boxPatrocionadores = true;
        $scope.boxItem = false;
        $scope.logout = logout;
        $scope.enderecoEmpresa = "";
        $scope.telefoneEmpresa = "";
        $scope.sliderSelect = sliderSelect;
        $scope.selectEmpresaDrop = selectEmpresaDrop;

        function selectEmpresaDrop(){
                    var id = $scope.selectDrop;
                      EmpresaService.getById(id).then(function(res){
                        if(res.data.length > 0){ 
                          $scope.enderecoEmpresa = res.data[0].endereco;
                          $scope.telefoneEmpresa = res.data[0].telefone;
                          ItemEmpresaService.getAllByIdEmpresa(res.data[0].id).then(function(response){
                            $scope.itens = response.data;
                            for(var i = 0; i < $scope.itens.length; i++){
                              $scope.itens[i].imagem = SettingsHelper.BaseUrl + "/admin/images/" + $scope.itens[i].imagem;
                            }
                            $scope.boxPatrocionadores = false;
                            $scope.boxItem = true;
                            $scope.searchteste = "";
                          }).catch(function(err){
                            console.log(err);
                            $rootScope.loader = false;
                          });
                        }
                        $rootScope.loader = false;
                      }).catch(function(err){
                        console.log(err);
                        $rootScope.loader = false;
                      });
        }

        var map;
        initialize();
        function initialize() {

          //select2
          // $(".selectEmpresaDrop").select2({
          //   placeholder: "Select a option"
          // });


          PatrocionadorService.getAll().then(function(res){
            $scope.patrocionadores = res.data;
            for(var i = 0; i < $scope.patrocionadores.length; i++){
              $scope.patrocionadores[i].imagem = SettingsHelper.BaseUrl + "/admin/images/" + 
              $scope.patrocionadores[i].imagem;
            }
          }).catch(function(err){
            console.log("falha ao getAll patrocionador");
          });
          var bounds = new google.maps.LatLngBounds();
          var latlngbounds = new google.maps.LatLngBounds();
          var infowindow = new google.maps.InfoWindow();
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13
          });
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
              var pos = { 
                lat: position.coords.latitude,
                lng: position.coords.longitude
              };
              map.setCenter(pos);
              var marker1 = new google.maps.Marker({
                position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                map: map
              }); 
              google.maps.event.addListener(marker1, 'click', (function (marker1) {
                return function () {
                  var content = "<h5>Você está aqui!</h5>";
                  infowindow.setContent(content);
                  infowindow.open(map, marker1);
                  $scope.itens = [];
                  $scope.boxItem = false;
                }
              })(marker1));
              bounds.extend(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
              map.fitBounds(bounds);
            });
          }
          // EmpresaService.getAll().then(function(response){
          //   console.log(response);
          // }).catch(function(err){
          //   console.log("erro");
          // })
          $rootScope.loader = true;

          EmpresaService.getAll().then(function(response){
              $scope.empresas = response.data;
              for(var i = 0; i < $scope.empresas.length; i++){
                // var image = {
                //   url: SettingsHelper.BaseUrl + "/admin/images/" +$scope.empresas[i].imagem,
                //   // This marker is 20 pixels wide by 32 pixels high.
                //   size: new google.maps.Size(42, 42),
                //   // The origin for this image is (0, 0).
                //   origin: new google.maps.Point(0, 0),
                //   // The anchor for this image is the base of the flagpole at (0, 32).
                //   anchor: new google.maps.Point(0, 32),
                //   shape:{coords:[17,17,18],type:'circle'}
                // };
                
                var image = new google.maps.MarkerImage(
                    SettingsHelper.BaseUrl + "/admin/images/" +$scope.empresas[i].imagem,
                    null,  
                    null, 
                    null,
                    new google.maps.Size(32, 32)
                );  
                var marker = new google.maps.Marker({
                  position: new google.maps.LatLng($scope.empresas[i].lat, $scope.empresas[i].lng),
                  title: $scope.empresas[i].nome,
                  map: map,
                  icon: image
                }); 
                
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                
                    return function () {

                      var iwContent = ""+
                      "<div>"+
                        "<h5>"+$scope.empresas[i].nome+"</h5>"+
                      "</div>";
                
                      infowindow.setContent(iwContent);
                      infowindow.open(map, marker);
                      $rootScope.loader = true;
                      ItemEmpresaService.getAllByIdEmpresa($scope.empresas[i].id).then(function(response){
                        $scope.itens = response.data;
                        for(var i = 0; i < $scope.itens.length; i++){
                          $scope.itens[i].imagem = SettingsHelper.BaseUrl + "/admin/images/" + $scope.itens[i].imagem;
                        }
                        $scope.enderecoEmpresa = $scope.empresas[i].endereco;
                        $scope.telefoneEmpresa = $scope.empresas[i].telefone;
                        $scope.boxPatrocionadores = false;
                        $scope.boxItem = true;
                        $rootScope.loader = false;
                      }).catch(function(err){
                        console.log(err);
                      });

                    }

                })(marker, i));
                
                bounds.extend(new google.maps.LatLng($scope.empresas[i].lat, $scope.empresas[i].lng));
                map.fitBounds(bounds);

              }
            //map.fitBounds(latlngbounds);
            $rootScope.loader = false;
          }).catch(function(data){
              console.log(data);
          });
          //$(".patrocionadores").owlCarousel();
        }

        $scope.fechaItem = function(){
          $scope.boxItem = false;
          $scope.boxPatrocionadores = true;
        }

        function sliderSelect(item){
          $rootScope.loader = true;
          EmpresaService.getById(item.id).then(function(res){
            if(res.data.length > 0){ 
              $scope.enderecoEmpresa = res.data[0].endereco;
              $scope.telefoneEmpresa = res.data[0].telefone;
              ItemEmpresaService.getAllByIdEmpresa(res.data[0].id).then(function(response){
                $scope.itens = response.data;
                for(var i = 0; i < $scope.itens.length; i++){
                  $scope.itens[i].imagem = SettingsHelper.BaseUrl + "/admin/images/" + $scope.itens[i].imagem;
                }
                $scope.boxPatrocionadores = false;
                $scope.boxItem = true;
              }).catch(function(err){
                console.log(err);
                $rootScope.loader = false;
              });
            }
            $rootScope.loader = false;
          }).catch(function(err){
            console.log(err);
            $rootScope.loader = false;
          });
        }

        function logout(){
            localStorageService.remove("propulsion");
            //$state.go('login');
            navigator.app.exitApp();
        }


    }

}());