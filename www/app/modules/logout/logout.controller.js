﻿(function () {
    'use strict';

    angular
        .module("app")
        .controller("LogoutController", LogoutController);

    LogoutController.$inject = ['LoginService', '$state'];
    function LogoutController(LoginService, $state) {
        var vm = this;

        ///////////////////////////////////////////////////////////////////////////

        activate();

        //////////////////////////////////////////////////////////////////////////

        function activate() {
            LoginService.logout();
            $state.go("login");
        }

    }

}());