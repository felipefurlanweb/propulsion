﻿(function () {
    'use strict';
    angular
        .module('app')
        .factory('SettingsHelper', SettingsHelper);

    SettingsHelper.$inject = [];
    function SettingsHelper() {
        var service = {
            BaseUrl: 'http://cswsistemas.com/propulsion',
            //BaseUrl: 'http://localhost/propulsion',
            StorageName: 'APPGEO',
            CurrentLanguage: 'pt-BR'
        };

        return service;
    }

}());