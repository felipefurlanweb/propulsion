﻿(function () {
	'use strict';

	angular
		.module("app")
		.run(run);

	run.$inject = ['$rootScope','$state', 'LoginService'];
	function run($rootScope, $state, LoginService) {

	    $rootScope.loader = false;

		$rootScope.$on("$stateChangeError", function (event, current, previous, eventObj) {
			if (!eventObj.isAuth) {
				event.preventDefault();
				$state.go("login");
			}
		});
	}


}());
