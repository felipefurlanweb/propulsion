﻿(function () {
    'use strict';
    angular
        .module('app.core', [
            'ui.router',
            'angularValidator',
            'LocalStorageModule',
            'ui.select2'
        ]);
}());