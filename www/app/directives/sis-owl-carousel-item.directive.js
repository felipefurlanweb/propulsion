﻿(function (app) {
    app.directive('sisOwlCarouselItem', sisOwlCarouselItem)
    sisOwlCarouselItem.$inject = [];
    function sisOwlCarouselItem() {
        return {
            restrict: 'A',
            link: function ($scope, $element) {
                if ($scope.$last) {
                    $scope.startOwlCarousel($element.parent());
                }
            }
        }
    }
}(angular.module('app.core')))