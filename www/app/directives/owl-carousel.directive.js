﻿(function (app) {
    app.directive('sisOwlCarousel', sisOwlCarousel)
    sisOwlCarousel.$inject = [];
    function sisOwlCarousel() {
        return {
            restrict: 'A',
            link: function ($scope, $element) {
                $scope.startOwlCarousel = function (elem) {
                    $("."+$element[0].className).owlCarousel({
                        autoPlay: true,
                        timeout: 3000
                    });
                }
            }
        }


    }
}(angular.module('app.core')))