﻿(function () {
    'use strict';
    angular
        .module('app')
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $urlRouterProvider.otherwise('login');

        $stateProvider
            .state('login',
            {
                url: '/login',
                templateUrl: "app/modules/login/login.html",
                controller: "LoginController",
                data: { pageTitle: 'Página de Login' },
                resolve: {

                }
            })
            .state('logout',
            {
                url: '/logout',
                controller: "LogoutController",
                controllerAs: 'logoutCtrl',
                resolve: {

                }
            })
            .state('home',
            {
                url: '/home',
                templateUrl: "app/modules/home/index.html",
                controller: "IndexController",
                data: { pageTitle: 'Home' },
                resolve: {
                    
                }
            })
            .state('usuarios',
            {
                url: '/usuarios',
                templateUrl: "app/modules/usuarios/usuarios.html",
                controller: "UsuariosController",
                controllerAs: 'usuariosCtrl',
                data: { pageTitle: 'Usuarios' },
                resolve: {

                }
            })

    }

    isAuth.$inject = ['LoginService','$q'];
    function isAuth(LoginService, $q) {
        var deferred = $q.defer();
        if (!LoginService.isAuthenticate()) {
            deferred.reject({isAuth: false});
        } else {
            deferred.resolve({ isAuth: true });
        }
        return deferred.promise;
    }

}());